import React, { Component } from 'react'
import { Map, TileLayer, Polyline, CircleMarker } from 'react-leaflet';
import './Map.css'
import jsonFile from './busLines.json'

class MapContainer extends Component {

  state = {
    centerCoordinates: [51.35, 19.38],
    busLines: [],
    busCoords: null,
    motion: false,
    generatedColor: false,
    generatePointsNum: 1,
    motionPoints: [],
    circleColors: []
  }

  componentDidMount() {
    const geoJson = jsonFile
    const busGeometry = jsonFile.features.map(line => line.geometry.coordinates.map(item => item.map(coords => coords.reverse())))
    const geometry = {...geoJson, busGeometry}
    this.setState({
      busLines: geometry.features,
      generateColor: true
    })
  }

  toggleMove = () => {
    this.state.motion ?
    this.moveOff()
    :
    this.moveOn()
  }

  moveOn = () => {
    const lines = this.state.busLines
    this.busMove = setInterval(() => {
      const nextMove = this.state.motionPoints.map((set, index) => set.map(item => item >= lines[index].geometry.coordinates.length - 1 ? 0 : item + 1))
      const randomBusCoordinates = nextMove.map((item, index) => item.map(coords => lines[index].geometry.coordinates[coords][0]));
      const buses = randomBusCoordinates.map((line, index) => line.map(values => ({ coords: values, busLine: lines[index].properties.ref })))
      this.setState({
        motion: true,
        busCoords: buses,
        motionPoints: nextMove
      })
    }, 1000);
  }

  moveOff = () => {
    this.setState({
      motion: false
    })
    clearInterval(this.busMove)
  }

  toggleBuses = () => {
    this.moveOff()
    const result = this.getRandomPositions()
    const randomBusCoordinates = result.map((item, index) => item.map(coords => this.state.busLines[index].geometry.coordinates[coords][0]))
    const buses = randomBusCoordinates.map((line, index) => line.map(values => ({ coords: values, busLine: this.state.busLines[index].properties.ref })))
    this.setState({
      busCoords: !this.state.busCoords ? buses : null,
      generatedColor: !this.state.generatedColor,
      motionPoints: result,
    });
    this.generateColors()
  }

  getRandomPositions = () => {
    const generateNumber = this.state.generatePointsNum;
    let result = []
    this.state.busLines.forEach(line => {
      let array = []
      for (let i = 0; i < generateNumber; i++) {
        array.push(this.getRandomNum(0, line.geometry.coordinates.length - 1))
      }
      result.push(array)
    });
    return result
  }

  getRandomNum = (min, max) => {
   return Math.round(Math.random() * (max - min) + min);
  }

  generateColor = () => {
    const rgb = `rgb(${ this.getRandomNum(0, 255) }, ${ this.getRandomNum(0, 255) }, ${ this.getRandomNum(0, 255) })`
    return rgb
  }

  handleChangeNumber = (e) => {
    this.setState({
      generatePointsNum: e.target.value
    })
  }

  generateColors = () => {
    const circleColors = this.state.busLines.map(bus => this.generateColor());
    this.setState({
      circleColors
    })
  }

  render() {
    const defaultColor = 'rgba(115,151,255, 0.5)'
    return(
      <div id="map-container">
        <Map center={this.state.centerCoordinates}
             zoom={13}
             viewport={this.state.centerCoordinates}
        >
        {
          this.state.busLines.map(line => 
            <Polyline key={line.properties.Name}
                      positions={line.geometry.coordinates.map(set => set.map(coords => coords))}
                      color={ defaultColor }
            />
          )
        }
        {
          this.state.busCoords ?
          this.state.busCoords.map((coords, index) => coords.map((bus, indexTwo) => 
            <CircleMarker key={index + indexTwo}
                          center={bus.coords}
                          radius={6}
                          fillColor={ this.state.circleColors[index] }
                          fillOpacity={0.75}
                          weight={1}
                          color={'#000'}
            />
          ))
          :
          null
        }
          <TileLayer url="http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png"
                     attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          />
        </Map>
        <div className="navigation">
        
        <div>
        
          <button onClick={this.toggleBuses}>
          
          {
            this.state.busCoords ? 
            'Hide Buses'
            :
            'Show Buses'
          }
          
          </button>

          <button onClick={this.toggleMove}>
          
          {
            this.state.motion ? 
            'Stop moving'
            :
            'Start moving'
          }
          
          </button>
          </div>
          <div className="input">
            <p>
              Generate points per bus line:
            </p>
            <input value={this.state.generatePointsNum}
                   onChange={this.handleChangeNumber}
                   type="number"
            />
            <p>
              Buses to generate: {this.state.busLines.length * this.state.generatePointsNum}
            </p>
           </div>
        </div>
      </div>
    )
  }
}

export default MapContainer