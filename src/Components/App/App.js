import React, { Component } from 'react';
import './App.css';
import MapContainer from './../MapContainer/MapContainer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1 className="app-title">Bełchatów MZKS</h1>
        <MapContainer />
      </div>
    );
  }
}

export default App;
